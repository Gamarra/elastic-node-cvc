export interface Offer {
  id: string;
  origin: string;
  destination: string;
  departure: string;
  returning: string;
  airline: string;
  totalPrice: string;
  domestic: string;
  creationDateTime: string;
  legsAmount: string;
  cabin: string;
};