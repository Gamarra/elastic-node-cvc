import bodyParser from "body-parser";
import https from "https";
import express, { Express, Request, Response } from 'express';

import elasticsearch from "elasticsearch";
import elasticIndexMappingsJSON from "./elasticIndexMappings.json";
import { Offer } from "./offers";

const sls = require("serverless-http");

const app: Express = express()
app.use(bodyParser.json())

const ELASTIC_INDEX: string = "fltoffers-offers-1";
const ELASTIC_URL_OFFERS: string = "vpc-aereo01-ti-mdzwjrstk5tx2vyi5i3dbsicg4.sa-east-1.es.amazonaws.com";
const MAX_SIZE_RESULTS: number = 100;

const implementedFilters = [
  "flightType",
  "origin", 
  "destination", 
  "itinerarySegments", 
  "fromCountry", 
  "toCountry", 
  "domestic", 
  "departure", 
  "returning", 
  "itineraryDates", 
  "departureMonth", 
  "stayDuration", 
  "airline", 
  "airlineName", 
  "legsAmount", 
  "baggage", 
  "cabin", 
  "company", 
  "channel", 
  "source"
];

const esClient = new elasticsearch.Client({
  host: ELASTIC_URL_OFFERS,
});

// *****************
//   CONTROLLERS
// *****************
app.get("/offers", (req: Request, res: Response) => {
  const mustList: {}[] = buildQuery(req);
  const size: number = req.query.size ? parseInt(req.query.size.toString()) : MAX_SIZE_RESULTS;
  const options = getESGetOffersOptions(mustList, size);

  esClient.search<Offer>(options).then((response) => {
    const offers: Offer[] = response.hits.hits.map(offerHit => offerHit._source);
    const offersResponse = buildOffersResponse(response.hits.total, size, offers);
    return res.json(offersResponse);
  }).catch(err => {
    return res.status(500).json({ "message": "Error" + err.toString() })
  })
});

const buildQuery = (req: Request) => {
  const mustList: {}[] = [];
  implementedFilters.forEach(filter =>  {
    if ( req.query[filter]){
      mustList.push({ match: { [filter]: req.query[filter] } })
    }
  });
  return mustList;
}

const getESGetOffersOptions = (mustList: any[], size: number) => {
  return {
    index: ELASTIC_INDEX,
    body: {
      size: size,
      query: {
        bool: {
          must: mustList,
          adjust_pure_negative: true,
          boost: 1
        }
      }
    }
  }
}

const buildOffersResponse = (total: number, size: number, offers: Offer[]) => {
  return {
    pagination: {
      offset: 0, // TODO: offset feature
      limit: size,
      total: total,
    },
    offers: offers
  }
}

// Create Index
app.post('/offers/create_index', (request, response) => {
  const elasticIndexMappings = JSON.stringify(elasticIndexMappingsJSON)
  const options = getESCreateIndexOptions(elasticIndexMappings.length);

  const req = https.request(options, res => {
    console.log(`statusCode: ${res.statusCode}`)
    res.on('elasticIndexMappings', d => {
      process.stdout.write(d)
    })
  })

  req.on('error', error => {
    console.error(error)
  })

  req.write(elasticIndexMappings)
  req.end()
});

const getESCreateIndexOptions = (contentLength: number) => {
  return {
    hostname: ELASTIC_URL_OFFERS,
    port: 443,
    path: "/" + ELASTIC_INDEX,
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': contentLength
    }
  }
}

app.post('/offers', (request, response) => {
  const options = getESPostOfferOptions();

  const req = https.request(options, res => {
    console.log(`statusCode: ${res.statusCode}`)
    res.on('elasticAddDocument', d => {
      process.stdout.write(d)
    })
  })

  req.on('error', error => {
    console.error(error)
  })

  req.write(JSON.stringify(request.body))
  req.end()
});

const getESPostOfferOptions = () => {
  const ELASTIC_INDEX_URL = "/" + ELASTIC_INDEX + "/_doc";
  return {
    hostname: ELASTIC_URL_OFFERS,
    port: 443,
    path: ELASTIC_INDEX_URL,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    }
  };
}


module.exports.handler = sls(app);
